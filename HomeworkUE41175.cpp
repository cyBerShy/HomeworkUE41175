﻿// HomeworkUE41175.cpp :
// Научиться работать с массивами данных и преобразовывать типы данных.

#include "MyLittleHelpers.h" // мои вспомогательные функции
#include <iostream>
#include <iomanip> // необходимо для выравнивания таблицы
#include <math.h> // математические функции

HANDLE hConsole; //нужно для получения дескриптора окна консоли
CONSOLE_SCREEN_BUFFER_INFO csbiInfo; //получение атрибутов консоли (вроде бы)
WORD wOldColorAttrs; //переменная для хранения параметров по умолчанию для консоли


enum ColorConsole {  //список доступных цветов для косноли
    Black = 0,
    Blue = 1,
    Green = 2,
    Cyan = 3,
    Red = 4,
    Magenta = 5,
    Brown = 6,
    LightGray = 7,
    Gray = 8,
    LightBlue = 9,
    LightGreen = 10,
    LightCyan = 11,
    LightRed = 12,
    LightMagenta = 13,
    Yellow = 14,
    White = 15
};

class Vector
{
private:

    double x;
    double y;
    double z;
    double length;

public:
    Vector() : x(0), y(0), z(0)  //инициализация по умолчанию
    {}

    Vector(double _x, double _y, double _z) : x(_x), y(_y), z(_z)  //инициализация переданными значениями
    {}

    void Show() // демонстрация значений вектора
    {

        SetConsoleTextAttribute(hConsole, (((Black << 4) | LightCyan))); //задаем цвет фона | цвет текста
        std::cout << "Вектор ";
        SetConsoleTextAttribute(hConsole, wOldColorAttrs); //возвращаем все как было
        std::cout << "]===>-->===[ ";
        SetConsoleTextAttribute(hConsole, (((Black << 4) | LightRed)));
        std::cout << "x = " << std::setw(3) << x;
        SetConsoleTextAttribute(hConsole, wOldColorAttrs);
        std::cout << " ]=[ ";
        SetConsoleTextAttribute(hConsole, (((Black << 4) | LightGreen)));
        std::cout << "y = " << std::setw(3) << y;
        SetConsoleTextAttribute(hConsole, wOldColorAttrs);
        std::cout << " ]=[ ";
        SetConsoleTextAttribute(hConsole, (((Black << 4) | LightBlue)));
        std::cout << "z = " << std::setw(3) << z;
        SetConsoleTextAttribute(hConsole, wOldColorAttrs);
        std::cout << " ]" << std::endl;

    }

    void Length()  // демонстрация длины (модуля) вектора
    {
        length = sqrt(pow(x, 2)+ pow(y, 2)+ pow(z, 2)); // корень квадратный из суммы квадратов координат вектора

        std::cout << "Длина ";
        SetConsoleTextAttribute(hConsole, (((Black << 4) | LightCyan))); //задаем цвет фона | цвет текста
        std::cout << "вектора ";
        SetConsoleTextAttribute(hConsole, wOldColorAttrs); //возвращаем все как было
        std::cout << " ] = -= [";
        SetConsoleTextAttribute(hConsole, (((Black << 4) | Yellow)));
        std::cout << std::setw(8) << length;
        SetConsoleTextAttribute(hConsole, wOldColorAttrs);
        std::cout << "]" << std::endl;

    }
};

int main()
{
    ConsoleCP cp(1251); //переключаем ввод и вывод консоли на кириллицу

    hConsole = GetStdHandle(STD_OUTPUT_HANDLE); //получаем дескриптор консоли, в которой нужно изменять цвет
    GetConsoleScreenBufferInfo(hConsole, &csbiInfo); //извлечение текущих параметров консоли (если правильно понял)
    wOldColorAttrs = csbiInfo.wAttributes; //запоминаем текущий цвет вывода консоли

    Vector Ve1;
    Vector Ve2(7, 55, 110);
    
    std::cout << "Первый вектор:\n";
    Ve1.Show();
    Ve1.Length();
    std::cout << '\n';
    std::cout << "Второй вектор:\n";
    Ve2.Show();
    Ve2.Length();

    SetConsoleTextAttribute(hConsole, wOldColorAttrs); //на всякий случай, возвращаем все как было

    return 0;
}

